"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  newmodel: {},
  bus: (0, _service.inject)("pubsub"),
  cardno: undefined,
  bankid: undefined,
  bankname: undefined,
  init: function init() {
    this._super();

    this.get('bus').on('cardno', this, this.recivedCardData);
  },
  recivedCardData: function recivedCardData(data, bankid, bankname) {
    console.log("init payment");
    this.set("cardno", data);
    this.set("bankid", bankid);
    this.set("bankname", bankname);
  },
  actions: {
    discardroute: function discardroute() {
      this.transitionToRoute('home');
    },
    pay: function pay(newmodel) {
      var newtransaction = this.get("store").createRecord("transaction");
      var public_key = '-----BEGIN PUBLIC KEY-----';
      public_key += 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXilNUfc4p9pT5rFJCCgf2Afuw';
      public_key += 'hd5mKbxDXvokeIcjjlSHCw5Xh+P/nRhCtYzOTc6ApaRNLl6F0rG66YUUkaUrN7RD';
      public_key += 'ZQn3oXoLKkWBiOSDjP1UL3ijyA5hV+rZWwOfGIZMtJMn0F9KjEDV97kTuf/6NHK+';
      public_key += 'FX/pt4YWDupNuBF7HwIDAQAB';
      public_key += '-----END PUBLIC KEY-----';
      var publicKey = forge.pki.publicKeyFromPem(public_key);

      if (this.get('cardno') !== undefined && newmodel.cvv !== undefined && newmodel.expmm !== undefined && newmodel.expyy !== undefined && newmodel.finalamount !== undefined) {
        var encryptedcardno = publicKey.encrypt(this.get('cardno'), "RSA-OAEP", {
          md: forge.md.sha256.create(),
          mgf1: forge.mgf1.create()
        });
        var encryptedcvv = publicKey.encrypt(newmodel.cvv, "RSA-OAEP", {
          md: forge.md.sha256.create(),
          mgf1: forge.mgf1.create()
        });
        var encryptedmm = publicKey.encrypt(newmodel.expmm, "RSA-OAEP", {
          md: forge.md.sha256.create(),
          mgf1: forge.mgf1.create()
        });
        var encryptedyy = publicKey.encrypt(newmodel.expyy, "RSA-OAEP", {
          md: forge.md.sha256.create(),
          mgf1: forge.mgf1.create()
        });
        newtransaction.set("cardno", encryptedcardno);
        newtransaction.set("cvv", encryptedcvv);
        newtransaction.set("cardtype", newmodel.cardtype);
        newtransaction.set("expmm", encryptedmm);
        newtransaction.set("expyy", encryptedyy);
        newtransaction.set("bankid", this.get('bankid'));
        newtransaction.set("bankname", this.get('bankname'));
        newtransaction.set("finalamount", newmodel.finalamount);
        newtransaction.set("datetimestamp", Math.round(new Date().getTime() / 1000));
        newtransaction.set("datetimestampstr", new Date());
        newtransaction.set("datetimestampstr", new Date());
        var requestUrl = "http://ip-api.com/json";
        $.ajax({
          url: requestUrl,
          type: 'GET',
          success: function success(json) {
            var ip = json.query;
            console.log(json);
            newtransaction.set("country", json.country);
            newtransaction.set("countrycode", json.countryCode);
            var region = json.regionName + " , " + json.city;
            newtransaction.set("regionname", region);
            newtransaction.set("zip", json.zip);
            newtransaction.set("lat", json.lat);
            newtransaction.set("lng", json.lon);
            newtransaction.set("timezone", json.timezone);
            newtransaction.set("ip", ip);
            newtransaction.set("isp", json.isp);
            newtransaction.set("extraone", json.org);
            newtransaction.set("extratwo", json.as);
            newtransaction.set("extrathree", "N/A");
            newtransaction.set("extrathree", "N/A");
            newtransaction.set("errormsg", "N/A");
            newtransaction.set("paymenttype", "payment_request");
            newtransaction.set("statuscode", "REQ0001");
            newtransaction.set("paycentpct", 0.00);
            newtransaction.set("bankpct", 0.00);
            newtransaction.set("merchantpct", 0.00);
            newtransaction.set("priority", 0.00);
            var requestUrl2 = "http://ip-api.com/json/" + ip + "?fields=currency";
            $.ajax({
              url: requestUrl2,
              type: 'GET',
              success: function success(json2) {
                newtransaction.set("currency", json2.currency);
              }
            });
          },
          error: function error(err) {
            console.log("Request failed, error= " + err);
          }
        });
      } else {}
    }
  }
});

exports["default"] = _default;