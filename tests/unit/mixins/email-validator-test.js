import EmberObject from '@ember/object';
import EmailValidatorMixin from 'frontendnew/mixins/email-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | email-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let EmailValidatorObject = EmberObject.extend(EmailValidatorMixin);
    let subject = EmailValidatorObject.create();
    assert.ok(subject);
  });
});
