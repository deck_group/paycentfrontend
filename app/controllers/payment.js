import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
    newmodel: {},
    bus: service("pubsub"),
    cardno: undefined,
    bankid: undefined,
    bankname: undefined,
    init() {
        this._super();
        this.get('bus').on('cardno', this, this.recivedCardData);
    },
    recivedCardData(data, bankid, bankname) {
        this.set("cardno", data)
        this.set("bankid", bankid)
        this.set("bankname", bankname)
    },
    actions: {
        discardroute() {
            this.transitionToRoute('home')
        },
        pay(newmodel) {
            var that = this
            var newtransaction = this.get("store").createRecord("transaction");
            var public_key = '-----BEGIN PUBLIC KEY-----';
            public_key += 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXilNUfc4p9pT5rFJCCgf2Afuw';
            public_key += 'hd5mKbxDXvokeIcjjlSHCw5Xh+P/nRhCtYzOTc6ApaRNLl6F0rG66YUUkaUrN7RD';
            public_key += 'ZQn3oXoLKkWBiOSDjP1UL3ijyA5hV+rZWwOfGIZMtJMn0F9KjEDV97kTuf/6NHK+';
            public_key += 'FX/pt4YWDupNuBF7HwIDAQAB';
            public_key += '-----END PUBLIC KEY-----';
            var publicKey = forge.pki.publicKeyFromPem(public_key);
            if (this.get('cardno') !== undefined && newmodel.cvv !== undefined && newmodel.expmm !== undefined && newmodel.expyy !== undefined && newmodel.finalamount !== undefined) {
                var encryptedcardno = publicKey.encrypt(this.get('cardno'), "RSA-OAEP", {
                    md: forge.md.sha256.create(),
                    mgf1: forge.mgf1.create()
                });
                var encryptedcvv = publicKey.encrypt(newmodel.cvv, "RSA-OAEP", {
                    md: forge.md.sha256.create(),
                    mgf1: forge.mgf1.create()
                });

                var encryptedmm = publicKey.encrypt(newmodel.expmm, "RSA-OAEP", {
                    md: forge.md.sha256.create(),
                    mgf1: forge.mgf1.create()
                });

                var encryptedyy = publicKey.encrypt(newmodel.expyy, "RSA-OAEP", {
                    md: forge.md.sha256.create(),
                    mgf1: forge.mgf1.create()
                });
                var base64cvv = forge.util.encode64(encryptedcvv)
                var base64expmm = forge.util.encode64(encryptedmm)
                var base64expyy = forge.util.encode64(encryptedyy)
                var base64cardno = forge.util.encode64(encryptedcardno)

                newtransaction.set("cardno", base64cardno)
                newtransaction.set("cvv", base64cvv)
                newtransaction.set("cardtype", newmodel.cardtype)
                newtransaction.set("expmm", base64expmm)
                newtransaction.set("expyy", base64expyy)
                newtransaction.set("bankid", this.get('bankid'))
                newtransaction.set("bankname", this.get('bankname'))
                newtransaction.set("finalamount", newmodel.finalamount)
                newtransaction.set("datetimestamp", Math.round((new Date()).getTime() / 1000))
                newtransaction.set("datetimestampstr", new Date())
                newtransaction.set("datetimestampstr", new Date())
                newtransaction.set("errormsg", `N/A`);
                newtransaction.set("paymenttype", "payment_request")
                newtransaction.set("statuscode", "REQ0001")
                newtransaction.set("paycentpct", 0.00)
                newtransaction.set("bankpct", 0.00)
                newtransaction.set("merchantpct", 0.00)
                newtransaction.set("priority", 0.00)
                newtransaction.save().then(() => {
                    if (newtransaction.statuscode === "200") {
                        that.set('newmodel', undefined)
                        that.transitionToRoute('home')
                    }
                })

            } else { }
        }
    }
});
