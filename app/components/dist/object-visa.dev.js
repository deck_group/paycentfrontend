"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

var _objectCardValidator = _interopRequireDefault(require("../mixins/object-card-validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend(_objectCardValidator["default"], {
  tagName: "td",
  bus: (0, _service.inject)("pubsub"),
  classNames: ["col-sm-12 col-xs-12 col-12"],
  classDiv: "col-sm-3",
  dataOne: undefined,
  dataTwo: undefined,
  dataThree: undefined,
  totalDataString: undefined,
  message: undefined,
  checkmodel: undefined,
  bankid: 9999,
  bankname: "N/A",
  cvvstruc: undefined,
  cvvlen: undefined,
  cardtype: undefined,
  classInput: "form-control input-txt-three",
  actions: {
    checkA: function checkA() {
      if (this.get("data").length === 4) {
        document.getElementById("b").focus();
      }
    },
    checkB: function checkB() {
      if (this.get("dataOne").length === 4) {
        document.getElementById("c").focus();
      }
    },
    checkC: function checkC() {
      if (this.get("dataTwo").length === 4) {
        document.getElementById("d").focus();
      }
    },
    checkD: function checkD() {
      if (this.get("dataThree").length === 4) {
        var totalData = this.get("data") + this.get("dataOne") + this.get("dataTwo") + this.get("dataThree");
        this.set("totalDataString", totalData);
        document.getElementById("d").blur();
      }
    },
    clearMessage: function clearMessage() {
      this.set('message', undefined);
    },
    validcard: function validcard() {
      var cardno = this.get('totalDataString');
      this.set('message', undefined);
      var chkmodel = this.get('checkmodel').content;

      for (var i = 0; i < chkmodel.length; i++) {
        if (this.get('bankid') === 9999) {
          if (chkmodel[i].__data.prefix === "4") {
            this.set('message', this.validate(cardno, chkmodel[i].__data.cardstructure, chkmodel[i].__data.length, 'Card No.'));
            this.set('cvvstruc', chkmodel[i].__data.cvvpattern);
            this.set('cvvlen', chkmodel[i].__data.cvvlength);
            this.set('cardtype', chkmodel[i].__data.cardtype);
          }
        } else {
          if (chkmodel[i].__data.bankid === this.get('bankid')) {
            this.set('bankname', chkmodel[i].__data.bankname);
            this.set('message', this.validate(cardno, chkmodel[i].__data.cardstructure, chkmodel[i].__data.length, 'Card No.'));
            this.set('cvvstruc', chkmodel[i].__data.cvvpattern);
            this.set('cvvlen', chkmodel[i].__data.cvvlength);
            this.set('cardtype', chkmodel[i].__data.cardtype);
          }
        }

        if (this.get('message') !== undefined) {
          if (this.get('message').length > 0) {
            this.set('data', undefined);
            this.set('dataOne', undefined);
            this.set('dataTwo', undefined);
            this.set('dataThree', undefined);
            this.get('bus').cardpublish(undefined, 999, 'N/A');
          } else {
            this.get('bus').cardpublish(cardno, this.get('bankid'), this.get('bankname'));
          }
        } else {
          this.get('bus').cardpublish(cardno, this.get('bankid'), this.get('bankname'));
        }

        this.get('bus').cvvPublish(this.get('cvvstruc'), this.get('cvvlen'));
      }
    }
  }
});

exports["default"] = _default;