import Mixin from '@ember/object/mixin';

export default Mixin.create({
    validatecvv: function (value, regexformat, length, label) {
        var msgArray = []
        msgArray.push(this.formatRule(value, regexformat, label))
        msgArray.push(this.length(value, length, label))
        var filtered = msgArray.filter(function (el) {
            return el != "";
        });

        return filtered
    },
    formatRule(value, regexformat, label) {
        var msg = ''
        var patt = new RegExp(regexformat)
        if (patt.test(value) === false) {
            msg = label + ` format is not correct`
        }
        return msg

    },

    length(value, length, label) {
        var msg = ''
        if (value.length !== length) {
            msg = label + ` Not correct length`
        }
        return msg

    },
});
