import EmberObject from '@ember/object';
import ObjEmailUsernameMixin from 'frontendnew/mixins/obj-email-username';
import { module, test } from 'qunit';

module('Unit | Mixin | obj-email-username', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ObjEmailUsernameObject = EmberObject.extend(ObjEmailUsernameMixin);
    let subject = ObjEmailUsernameObject.create();
    assert.ok(subject);
  });
});
