import Service from '@ember/service';
import config from '../config/environment';

export default Service.extend({
    // websocket: new WebSocket('ws://localhost:8080'),
    websocket: new WebSocket(config.WEB_HOST),
    loginpub: function (content) {
        var ws = this.get('websocket')
        var newdata = "login" + "-" + JSON.stringify(content)
        ws.send(newdata)
    },
});
