import Component from '@ember/component';

export default Component.extend({
    classOne: `line-input`,
    name: `username`,
    tagname: `div`,
    classNames: [`form-group`],
    labeltag: ``,
    label: ``,
    dttype: `password`,
    visiblefl: false,
    classTwo: `reveal-password`,
    classPass: `passFor`,
    dimn: `24`,
    viewn: `0 0 24 24`,
    actions: {
        showpass() {
            if (this.get('visiblefl') === false) {
                this.set('visiblefl', true)
                this.set('dttype', 'text')
            }
            else {
                this.set('visiblefl', false)
                this.set('dttype', 'password')
            }
        }
    }
});
