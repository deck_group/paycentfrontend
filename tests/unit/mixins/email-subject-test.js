import EmberObject from '@ember/object';
import EmailSubjectMixin from 'frontendnew/mixins/email-subject';
import { module, test } from 'qunit';

module('Unit | Mixin | email-subject', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let EmailSubjectObject = EmberObject.extend(EmailSubjectMixin);
    let subject = EmailSubjectObject.create();
    assert.ok(subject);
  });
});
