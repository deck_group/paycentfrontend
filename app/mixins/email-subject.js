import Mixin from '@ember/object/mixin';

export default Mixin.create({
    validate: function (value, isPresence, label) {
        var msgArray = []
        if (isPresence === true) {
            msgArray.push(this.presence(value, label))
        }
        var filtered = msgArray.filter(function (el) {
            return el != "";
        });

        return filtered
    },

    presence(value, label) {
        var msg = ''
        if (value === undefined || value === "") {
            msg = label + ` is required`
        }
        return msg

    },
});
