'use strict';

module.exports = function (environment) {
  let ENV = {
    modulePrefix: 'frontendnew',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    contentSecurityPolicy: {
      'default-src': "'self'",
      'script-src': "'self' *.paycentfrontend-7le2ce7b-0mf4scmj-dev.eu-west-1.klovercloud.com",
      'font-src': "'self' fonts.googleapis.com fonts.gstatic.com",
      'connect-src': "'self' my-auth-provider.com",
      'img-src': "self",
      'style-src': "self",
      'media-src': "self"
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    ENV.API_HOST = 'http://localhost:8000'
    ENV.WEB_HOST = 'ws://localhost:8080'
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
    ENV.API_HOST = 'https://paycentbackend-1pgl74ep-vgdwhjge-dev.eu-west-1.klovercloud.com'
    ENV.WEB_HOST = 'wss://paygetisagain-es3kk9rn-4esed6t9-dev.eu-west-1.klovercloud.com'
  }

  return ENV;
};
