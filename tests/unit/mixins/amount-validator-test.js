import EmberObject from '@ember/object';
import AmountValidatorMixin from 'frontendnew/mixins/amount-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | amount-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let AmountValidatorObject = EmberObject.extend(AmountValidatorMixin);
    let subject = AmountValidatorObject.create();
    assert.ok(subject);
  });
});
