import Component from '@ember/component';
import objectcvvvalidator from '../mixins/object-cvv-validator';
import { inject as service } from '@ember/service';

export default Component.extend(objectcvvvalidator,{
    tagName: `div`,
    classNames: [`col-sm-12 form-group`],
    labeltag: ``,
    label: ``,
    isPresence: true,
    message: undefined,
    classDivThree: `row col-sm-8 margin-some`,
    classDivOne: `col-sm-2 paddingupper`,
    classDivTwo: `col-sm-10`,
    classOne: `line-input-email`,
    name: `cvv`,
    data: ``,
    bus: service("pubsub"),
    cvvflag: true,
    format: `^[0-9]*$`,
    cvvformat: ``,
    cvvlenn: 0,
    init() {
        this._super();
        this.get('bus').on('cvv', this, this.recievedcvv);
    },
    recievedcvv(struc, len) {
        console.log("hello")
        this.set('cvvformat', struc)
        this.set('cvvlen', len)
        this.set('cvvflag', false)
    },
    actions: {
        clearMessageCVV() {
            this.set('messagec', undefined)
        },
        validcvv(value) {
            this.set('message', undefined)
            this.set('message',
                this.validatecvv(value,
                    this.get('cvvformat'),
                    this.get('cvvlen'),
                    "CVV"
                ))

        },

    }
});
