import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
    scroll: '',
    scrollclass: computed('scroll', function () {
        return this.get('scroll')
    }),

    didInsertElement() {
        this._super(...arguments);
        var lastScroll = 0;
        var that = this
        window.onscroll = function () {
            let currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
            if (currentScroll > 0 && lastScroll <= currentScroll) {
                lastScroll = currentScroll;
                that.set('scroll', 'scrolled')
            } else {
                lastScroll = currentScroll;
                that.set('scroll', '')
                console.log("scroll up")
            }
        };
    }
});
