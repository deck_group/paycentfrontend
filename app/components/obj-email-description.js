import Component from '@ember/component';

export default Component.extend({
    tagName: `div`,
    classNames: [`col-sm-12 form-group`],
    labeltag: ``,
    label: ``,
    isPresence: true,
    message: undefined,
    classDivThree: `row col-sm-8 margin-some`,
    classDivOne: `col-sm-4 paddingupper`,
    classDivTwo: `col-sm-8`,
    classOne: `line-input-email-body`,
    name: `body`,
    data: ``,
});
