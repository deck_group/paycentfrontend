import DS from 'ember-data';
const {
  Model,
  attr
} = DS;

export default Model.extend({
  bankid: attr('number'),
  bankname: attr('string'),
  cardid: attr('number'),
  cardtype: attr('string'),
  name: attr('string'),
  rate: attr('number'),
  imagename: attr('string'),
  imageurl: attr('string'),
  prefix: attr('string'),
  imageData: attr('string'),
  length: attr('number'),
  cardstructure: attr('string'),
  cvvpattern: attr('string'),
  cvvlength: attr('number'),
});
