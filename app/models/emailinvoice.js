import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    email: attr('string'),
    username: attr('string'),
    amount: attr('number'),
    unit:attr('string'),
    description: attr('string'),
});
