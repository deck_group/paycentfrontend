import Service from '@ember/service';
import config from '../config/environment';

export default Service.extend({
    websocket: new WebSocket(config.WEB_HOST),
    emailpub: function (content) {
        var ws = this.get('websocket')
        var newdata = "invoiceemailmerchant" + "-" + JSON.stringify(content)
        ws.send(newdata)
    },
});
