import Component from '@ember/component';
import objectexpmm from '../mixins/object-exp-mm';
import objectexpyy from '../mixins/object-exp-yy';

export default Component.extend(objectexpmm, objectexpyy, {
    tagName: `div`,
    classNames: [`col-sm-12 form-group`],
    labeltag: ``,
    label: ``,
    isPresence: true,
    message: undefined,
    classDivThree: `row col-sm-8 margin-some`,
    classDivOne: `col-sm-4 paddingupper`,
    classDivTwo: `col-sm-4`,
    classOne: `line-input-email`,
    messagemm: ``,
    messageyy: ``,
    yydata: undefined,
    mmdata: undefined,
    actions: {
        clearMessageYY() {
            this.set('messageyy', undefined)
        },
        clearMessageMM() {
            this.set('messagemm', undefined)
        },
        validexpyear(value) {
            this.set('messageyy', undefined)
            if (value !== undefined) {
                this.set('messageyy',
                    this.validateYear(value,
                        this.get('format'),
                        "Expiry YY"
                    ))
            }
        },
        validexpmonth(value) {
            this.set('messagemm', undefined)
            this.set('messagemm',
                this.validate(value,
                    this.get('format'),
                    "Expiry MM"
                ))
            if (this.get('messagemm').length === 0) {
                console.log("else")
                document.getElementById('yy').focus()
            } else {
                this.set("mmdata", undefined)
            }

        }

    }
});
