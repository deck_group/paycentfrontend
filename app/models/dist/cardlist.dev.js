"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Model = _emberData["default"].Model,
    attr = _emberData["default"].attr;

var _default = Model.extend({
  bankid: attr('number'),
  bankname: attr('string'),
  cardid: attr('number'),
  cardtype: attr('string'),
  name: attr('string'),
  rate: attr('number'),
  imagename: attr('string'),
  imageurl: attr('string'),
  prefix: attr('string'),
  imageData: attr('string'),
  length: attr('number'),
  cardstructure: attr('string'),
  cvvpattern: attr('string'),
  cvvlength: attr('number')
});

exports["default"] = _default;