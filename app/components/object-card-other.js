import Component from '@ember/component';
import objectexpmm from '../mixins/object-exp-mm';
import objectexpyy from '../mixins/object-exp-yy';
import objectcvvvalidator from '../mixins/object-cvv-validator';
import { inject as service } from '@ember/service';

export default Component.extend(objectexpmm, objectexpyy, objectcvvvalidator, {
    tagName: `div`,
    classNames: [`col-sm-12 col-xs-12 col-12 margin-br`],
    divClassOne: `col-sm-4 padding-margin`,
    divClassTwo: `col-sm-8`,
    divClassThree: `col-xs-6 col-sm-5`,
    labelClass: `col-xs-12 col-sm-12 spanlabel`,
    placeholderCVV: undefined,
    bus: service("pubsub"),
    messagecvv: ``,
    messagemm: ``,
    messageyy: ``,
    cvvflag: true,
    format: `^[0-9]*$`,
    cvvformat: ``,
    cvvlenn: 0,
    classInput: `form-control input-txt-three`,
    init() {
        this._super();
        this.get('bus').on('cvv', this, this.recievedcvv);
    },
    recievedcvv(struc, len) {
        console.log("hello")
        this.set('cvvformat', struc)
        this.set('cvvlen', len)
        this.set('cvvflag', false)

    },

    actions: {
        clearMessageYY() {
            this.set('messageyy', undefined)
        },
        clearMessageMM() {
            this.set('messagemm', undefined)
        },
        clearMessageCVV() {
            this.set('messagecvv', undefined)
        },
        validcvv(value) {
            this.set('messagecvv', undefined)
            this.set('messagecvv',
                this.validatecvv(value,
                    this.get('cvvformat'),
                    this.get('cvvlen'),
                    "CVV"
                ))

            if (this.get('messagemm').length > 0) {
                this.set("model.expyy", undefined)
            }
        },
        validexpyear(value) {
            this.set('messageyy', undefined)
            this.set('messageyy',
                this.validateYear(value,
                    this.get('format'),
                    "Expiry YY"
                ))

            if (this.get('messagemm').length > 0) {
                this.set("model.expyy", undefined)
            }

        },
        validexpmonth(value) {
            this.set('messagemm', undefined)
            this.set('messagemm',
                this.validate(value,
                    this.get('format'),
                    "Expiry MM"
                ))
            if (this.get('messagemm').length === 0) {
                document.getElementById('yy').focus()
            } else {
                this.set("model.expmm", undefined)
            }

        }
    }


});
