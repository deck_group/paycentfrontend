import Mixin from '@ember/object/mixin';

export default Mixin.create({
    validate: function (value, regexformat, label) {
        var msgArray = []
        msgArray.push(this.formatRule(value, regexformat, label))
        msgArray.push(this.expmonthlength(value, label))
        msgArray.push(this.expmonth(value, label))

        var filtered = msgArray.filter(function (el) {
            return el != "";
        });

        return filtered
    },
    formatRule(value, regexformat, label) {
        var msg = ''
        var patt = new RegExp(regexformat)
        if (patt.test(value) === false) {
            msg = label + ` format is not correct`
        }
        return msg

    },

    expmonthlength(value, label) {
        var month = value
        var msg = ''
        if (month.length !== 2) {
            msg = `Month must be in MM , please check the ` + label
        }
        return msg
    },
    expmonth(value, label) {
        var month = value
        var msg = ''
        if (month > 12) {
            msg = `Not Valid ` + label
        }
        return msg
    }

});
