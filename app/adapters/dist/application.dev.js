"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _emberData["default"].JSONAPIAdapter.extend({
  // host:'http://localhost:8000',
  host: "https://paycentbackend-qik8qz4d-67vbc4qc-dev.eu-west-1.klovercloud.com",
  namespace: 'api/v1'
});

exports["default"] = _default;