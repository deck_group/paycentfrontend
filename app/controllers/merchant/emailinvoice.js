import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
    newmodel:{},
    allpub: service('alllog'),
    emailpub: service('emailpub'),
    actions:{
        send(model){
            console.log(localStorage.getItem('merchantid'))
            model['merchantid']=localStorage.getItem('merchantid')
            model['unit']='GMD'
            model['role']=localStorage.getItem('role')
            var dict = {}
            dict['userid'] = localStorage.getItem('user')
            dict['username'] = localStorage.getItem('username')
            dict['userrole'] = localStorage.getItem('role')
            dict['activitycat'] = "invoice email"
            dict['activitysubcat'] = "N/A"
            dict['status'] = "success"
            dict['statuscode'] = 200
            dict['variablebefore'] = model
            dict['variableafter'] = model
            dict['comment'] = 'N/A'
            dict['reason'] = 'success'
            dict['reasoncode'] = 200
            this.get("allpub").alllog(dict)
            this.get('emailpub').emailpub(model)
            this.set('newmodel',{})
            this.transitionToRoute('merchant.dashboard')
        }
    }
});
