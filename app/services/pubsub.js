import Service from '@ember/service';
import Evented from '@ember/object/evented';

export default Service.extend(Evented, {
    cardpublish: function (content,bankid,bankname) {
        this.trigger('cardno', content,bankid,bankname)
    },
    cardTypePublish: function (content,bankid) {
        this.trigger('recievecardtype', content,bankid)
    },

    cvvPublish: function (content,len) {
        this.trigger('cvv', content,len)
    },
});
