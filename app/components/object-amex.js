import Component from '@ember/component';
import { inject as service } from '@ember/service';
import objectcardvalidator from '../mixins/object-card-validator';

export default Component.extend(objectcardvalidator, {
    tagName: `div`,
    bus: service("pubsub"),
    classDiv: `col-sm-3`,
    classDivTwo: `col-sm-6`,
    dataOne: undefined,
    dataTwo: undefined,
    totalDataString: undefined,
    totalDataStringNew:undefined,
    message: undefined,
    checkmodel: undefined,
    bankid: 9999,
    bankname: `N/A`,
    cvvstruc: undefined,
    cvvlen: undefined,
    cardtype:undefined,
    classInput: `input-txt-three`,
    actions: {
        checkA() {
            if ((this.get("data").length) === 4) {
                document.getElementById("b").focus()
            }
        },
        checkB() {
            if ((this.get("dataOne").length) === 6) {
                document.getElementById("c").focus()
            }
        },
        checkC() {
            if ((this.get("dataTwo").length) === 5) {
                var totalDataNew = this.get("data") + "-" + this.get("dataOne") + "-" + this.get("dataTwo")
                var totalData = this.get("data")  + this.get("dataOne") + this.get("dataTwo")
                this.set("totalDataString", totalData)
                this.set("totalDataStringNew", totalDataNew)
                document.getElementById("c").blur()

            }
        },
        clearMessage() {
            this.set('message', undefined)
        },
        validcard() {
            var cardno = this.get('totalDataString')
            var cardNew=this.get('totalDataStringNew')
            this.set('message', undefined)
            var chkmodel = this.get('checkmodel').content
            for (var i = 0; i < chkmodel.length; i++) {
                if (this.get('bankid') === 9999) {
                    if (chkmodel[i].__data.prefix === "37") {
                        this.set('message', this.validate(cardno, chkmodel[i].__data.cardstructure, chkmodel[i].__data.length, 'Card No.'))
                        this.set('cvvstruc', chkmodel[i].__data.cvvpattern)
                        this.set('cvvlen', chkmodel[i].__data.cvvlength)
                        this.set('cardtype', chkmodel[i].__data.cardtype)
                    }
                } else {
                    if (chkmodel[i].__data.bankid === this.get('bankid')) {
                        this.set('bankname', chkmodel[i].__data.bankname)
                        this.set('message', this.validate(cardno, chkmodel[i].__data.cardstructure, chkmodel[i].__data.length, 'Card No.'))
                        this.set('cvvstruc', chkmodel[i].__data.cvvpattern)
                        this.set('cvvlen', chkmodel[i].__data.cvvlength)
                        this.set('cardtype', chkmodel[i].__data.cardtype)
                    }
                }
                if (this.get('message') !== undefined) {
                    if (this.get('message').length > 0) {
                        this.set('data', undefined)
                        this.set('dataOne', undefined)
                        this.set('dataTwo', undefined)
                        this.set('dataThree', undefined)
                        this.get('bus').cardpublish(undefined);
                    }
                    else {
                        this.get('bus').cardpublish(cardNew, this.get('bankid'), this.get('bankname'));
                    }

                }
                else {

                    this.get('bus').cardpublish(cardNew, this.get('bankid'), this.get('bankname'));
                }
                this.get('bus').cvvPublish(this.get('cvvstruc'), this.get('cvvlen'));

            }


        }

    }


});
