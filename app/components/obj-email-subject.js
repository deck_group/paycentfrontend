import Component from '@ember/component';
import emailValidator from '../mixins/email-subject';

export default Component.extend(emailValidator,{
    tagName: `div`,
    classNames: [`col-sm-12 form-group`],
    labeltag: ``,
    label: ``,
    isPresence: true,
    message: undefined,
    classDivThree: `row col-sm-8 margin-some`,
    classDivOne: `col-sm-4 paddingupper`,
    classDivTwo: `col-sm-8`,
    classOne: `line-input-email`,
    name: `subject`,
    data: ``,
    actions: {
        clearMessage() {
            this.set('message', undefined)
        },
        checkSubject() {
            this.set('message', undefined)
            this.set('message',
                this.validate(this.get('data'),
                    this.get('isPresence'),
                    this.get('label')
                )
            )

        }
    }
});
