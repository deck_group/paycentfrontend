import Mixin from '@ember/object/mixin';

export default Mixin.create({
    validate: function (value, regexformat, length, label) {
        var msgArray = []
        msgArray.push(this.formatRule(value, regexformat, label))
        msgArray.push(this.length(value, length, label))
        msgArray.push(this.checkValid(value))

        var filtered = msgArray.filter(function (el) {
            return el != "";
        });

        return filtered
    },
    formatRule(value, regexformat, label) {
        var msg = ''
        var patt = new RegExp(regexformat)
        if (patt.test(value) === false) {
            msg = label + ` format is not correct`
        }
        return msg

    },

    length(value, length, label) {
        var msg = ''
        if (value.length !== length) {
            msg = label + ` Not correct length`
        }
        return msg

    },

    checkValid(cardno){
        var checksum = 0;
        var msg=``
        for (var x = (2 - (cardno.length % 2)); x <= cardno.length; x += 2) {
          checksum += parseInt(cardno.charAt(x - 1));
        }

        for (var i = (cardno.length % 2) + 1; i < cardno.length; i += 2) {
          var digit = parseInt(cardno.charAt(i - 1)) * 2;
          if (digit < 10) {
            checksum += digit;
          } else {
            checksum += (digit - 9);
          }
        }
        if ((checksum % 10) !== 0) {
            msg=`Not a valid card no`
        } 
        return msg
    }



});
