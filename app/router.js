import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function () {
  this.route('payment');
  this.route('login');
  this.route('home');
  this.route('newhome', { path: '/' });
  this.route('merchantsignin', { path: 'merchant-sign-in' });

  this.route('merchant', function () {
    this.route('dashboard', { path: 'merchant/dashboard' });
    this.route('emailinvoice', { path: 'merchant/user/email/invoice' });
  });

  this.route('user', function() {
    this.route('paymentwiseinvoice',{path: 'payment/:invoice_id'});
  });
});

export default Router;
