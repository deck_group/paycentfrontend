import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
    formclass: `form-group`,
    highlightclass: `col-sm-12 no-highlight`,
    rowclass: `row`,
    colclass: `col-sm-3`,
    colclasstwo: `col-sm-6`,
    btnclass: `btn btn-default`,
    btnLabel: `Sign In`,
    message: undefined,
    store: service(),
    router: service(),
    allpub: service('alllog'),
    loginpub: service('loginlog'),
    model: undefined,
    messageArray: computed('message', function () {
        return this.get('message')
    }),
    actions: {
        login(model) {
            this.set('message', undefined)
            var that = this
            this.get("store").query("logauth", {
                query: {
                    value1: model.loginid,
                    value: model.password
                }
            }).then(
                function (infomodel) {
                    if (infomodel.content.length === 0) {
                        that.set("message", ["Please check the Login ID or Password"])
                        that.get('messageArray')
                        var dict = {}
                        dict['userid'] = model.loginid
                        dict['username'] = "N/A"
                        dict['userrole'] = "N/A"
                        dict['activitycat'] = "login"
                        dict['activitysubcat'] = "N/A"
                        dict['status'] = "failed"
                        dict['statuscode'] = 400
                        dict['variablebefore'] = { 'loginid': model.loginid, 'password': model.password }
                        dict['variableafter'] = { 'loginid': model.loginid, 'password': model.password }
                        dict['comment'] = 'N/A'
                        dict['reason'] = 'Authentication failed'
                        dict['reasoncode'] = 300
                        that.get("allpub").alllog(dict)

                        var dicttow = {}
                        dicttow['userid'] = model.loginid
                        dicttow['username'] = "N/A"
                        dicttow['userrole'] = "N/A"
                        dicttow['activitytype'] = "login"
                        dicttow['inputuserid'] = model.loginid
                        dicttow['inputpassword'] = model.password
                        dicttow['lat'] = 'N/A'
                        dicttow['lng'] = 'N/A'
                        dicttow['ip'] = 'N/A'
                        dicttow['status'] = "failed"
                        dicttow['statuscode'] = 400
                        dicttow['variablebefore'] = { 'loginid': model.loginid, 'password': model.password }
                        dicttow['variableafter'] = { 'loginid': model.loginid, 'password': model.password }
                        dicttow['comment'] = 'N/A'
                        dicttow['reason'] = 'Authentication failed'
                        dicttow['reasoncode'] = 300
                        that.get("loginpub").loginpub(dicttow)

                    }
                    else {
                        infomodel.forEach(function (data) {
                            that.set('model', {})
                            localStorage.setItem('token', data.token)
                            localStorage.setItem('user', data.loginid)
                            localStorage.setItem('username', data.name)
                            localStorage.setItem('password', data.password)
                            localStorage.setItem('role', data.role)
                            localStorage.setItem('merchantid', data.merchantid)
                            var dict = {}
                            dict['userid'] = data.loginid
                            dict['username'] = data.name
                            dict['userrole'] = data.role
                            dict['activitycat'] = "login"
                            dict['activitysubcat'] = "N/A"
                            dict['status'] = "success"
                            dict['statuscode'] = 200
                            dict['variablebefore'] = { 'loginid': data.loginid, 'password': data.password }
                            dict['variableafter'] = { 'loginid': data.loginid, 'password': data.password }
                            dict['comment'] = 'N/A'
                            dict['reason'] = 'success'
                            dict['reasoncode'] = 200
                            that.get("allpub").alllog(dict)

                            var dicttow = {}
                            dicttow['userid'] = data.loginid
                            dicttow['username'] = data.name
                            dicttow['userrole'] = data.role
                            dicttow['activitytype'] = "login"
                            dicttow['inputuserid'] = data.loginid
                            dicttow['inputpassword'] = data.password
                            dicttow['lat'] = 'N/A'
                            dicttow['lng'] = 'N/A'
                            dicttow['ip'] = 'N/A'
                            dicttow['status'] = "success"
                            dicttow['statuscode'] = 200
                            dicttow['variablebefore'] = { 'loginid': data.loginid, 'password': data.password }
                            dicttow['variableafter'] = { 'loginid': data.loginid, 'password': data.password }
                            dicttow['comment'] = 'N/A'
                            dicttow['reason'] = 'success'
                            dicttow['reasoncode'] = 200
                            that.get("loginpub").loginpub(dicttow)



                            that.get('router').transitionTo('merchant.dashboard')
                        });
                    }
                }
            )
        }
    }
});
