import Component from '@ember/component';
import emailFieldValidator from '../mixins/email-validator';

export default Component.extend(emailFieldValidator, {
    tagName: `div`,
    classNames: [`col-sm-12 form-group`],
    labeltag: ``,
    label: ``,
    isPresence: true,
    message: undefined,
    classDivThree: `row col-sm-8 margin-some`,
    classDivOne: `col-sm-4 paddingupper`,
    classDivTwo: `col-sm-8`,
    classOne: `line-input-email`,
    format: `^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$`,
    name: `email address`,
    data:``,
    actions: {
        clearMessage() {
            this.set('message', undefined)
        },

        checkNumber() {
            this.set('message', undefined)
            this.set('message',
                this.validate(this.get('data'),
                    this.get('isPresence'),
                    this.get('label'),
                    this.get('format')
                )
            )

        }
    }

});
