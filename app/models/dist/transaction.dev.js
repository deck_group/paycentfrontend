"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Model = _emberData["default"].Model,
    attr = _emberData["default"].attr;

var _default = Model.extend({
  datetimestamp: attr('number'),
  datetimestampstr: attr('string'),
  finalamount: attr('string'),
  cvv: attr('string'),
  cardno: attr('string'),
  bankid: attr('number'),
  bankname: attr('string'),
  expmm: attr('string'),
  expyy: attr('string'),
  paymenttype: attr('string'),
  errormsg: attr('string'),
  statuscode: attr('string'),
  paycentpct: attr('number'),
  bankpct: attr('number'),
  merchantpct: attr('number'),
  priority: attr('number'),
  cardtype: attr('string'),
  country: attr('string'),
  countrycode: attr('string'),
  regionname: attr('string'),
  zip: attr('string'),
  lat: attr('string'),
  lng: attr('string'),
  timezone: attr('string'),
  ip: attr('string'),
  isp: attr('string'),
  extraone: attr('string'),
  extratwo: attr('string'),
  extrathree: attr('string'),
  currency: attr('string')
});

exports["default"] = _default;