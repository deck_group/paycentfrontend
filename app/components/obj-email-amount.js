import Component from '@ember/component';
import amountFieldValidator from '../mixins/amount-validator';

export default Component.extend(amountFieldValidator, {
    tagName: `div`,
    classNames: [`col-sm-12 form-group`],
    labeltag: ``,
    label: ``,
    isPresence: true,
    message: undefined,
    classDivfour: `row col-sm-8 margin-some`,
    classDivOne: `col-sm-4 paddingupper`,
    classDivTwo: `col-sm-7`,
    classDivThree: `col-sm-1 monetaryclass`,
    classOne: `line-input-email paddingupperone`,
    format: "^[0-9\\s.,]*$",
    name: `amount`,
    flag:false,
    data: ``,
    unit: ``,
    actions: {
        clearMessage() {
            this.set('message', undefined)
        },

        checkAmount() {
            this.set('message', undefined)
            this.set('message',
                this.validate(this.get('data'),
                    this.get('isPresence'),
                    this.get('label'),
                    this.get('format')
                )
            )

        }

    }
});
