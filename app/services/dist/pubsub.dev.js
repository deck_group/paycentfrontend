"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _service = _interopRequireDefault(require("@ember/service"));

var _evented = _interopRequireDefault(require("@ember/object/evented"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _service["default"].extend(_evented["default"], {
  cardpublish: function cardpublish(content, bankid, bankname) {
    console.log("card trig");
    this.trigger('cardno', content, bankid, bankname);
  },
  cardTypePublish: function cardTypePublish(content, bankid) {
    this.trigger('recievecardtype', content, bankid);
  },
  cvvPublish: function cvvPublish(content, len) {
    this.trigger('cvv', content, len);
  }
});

exports["default"] = _default;