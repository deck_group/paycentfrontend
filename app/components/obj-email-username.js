import Component from '@ember/component';
import userFieldValidator from '../mixins/obj-email-username';

export default Component.extend(userFieldValidator,{
    tagName: `div`,
    classNames: [`col-sm-12 form-group`],
    labeltag: ``,
    label: ``,
    isPresence: true,
    message: undefined,
    classDivThree: `row col-sm-8 margin-some`,
    classDivOne: `col-sm-4 paddingupper`,
    classDivTwo: `col-sm-8`,
    classOne: `line-input-email`,
    format:"^[a-zA-Z\\s.@-_']*$",
    name: `member name`,
    data:``,
    actions:{
        clearMessage() {
            this.set('message', undefined)
        },
        checkUsername() {
            this.set('message', undefined)
            this.set('message',
                this.validate(this.get('data'),
                    this.get('isPresence'),
                    this.get('label'),
                    this.get('format')
                )
            )

        }
    }
});
