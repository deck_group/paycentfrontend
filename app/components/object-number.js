import Component from '@ember/component';

export default Component.extend({
    tagName: `div`,
    classNames: [`col-sm-12 col-xs-12 col-12 margin-br`],
    classInput: `form-control input-txt-two`,
    placeholder:undefined
});
