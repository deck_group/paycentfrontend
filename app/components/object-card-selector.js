import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `div`,
    bus: service("pubsub"),
    classNames: [`scrolling maginsome`],
    divClass: `col-sm-4`,
    imageClass: `imagesizeCard`,
    imageClassDisabled:`imagesizeCardDis`,
    frontSymbol: `symbol-font`,
    store: service(),
    selectedid:undefined,
    modelname: undefined,
    model: computed(function () {
        return this.get('store').findAll(this.get('modelname'))
    }),
    actions: {
        selectedOption(type,bankid,id) {
            this.set('selectedid',id)
            this.get('bus').cardTypePublish(type,bankid);
        }
    }
});
