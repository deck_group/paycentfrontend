import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    datetimestamp: attr('string'),
    dt: attr('string'),
    time: attr('string'),
    userid: attr('string'),
    username: attr('string'),
    userrole: attr('string'),
    recievername: attr('string'),
    recieveremail: attr('string'),
    transactionid: attr('string'),
    unit: attr('string'),
    invoiceid: attr('string'),
    transactionstatus: attr('string'),
    transactionstatuscode: attr('string'),
    subject: attr('string'),
    message: attr('string'),
    amount: attr('string'),
    status: attr('string'),
    statuscode: attr('string')

});
