import Route from '@ember/routing/route';
import {
    hash
} from 'rsvp';

export default Route.extend({
    model(params){
        return hash({
            card: this.store.findAll('cardlist'),
            invoice:this.store.query('emailinvoicelog', {
                status: '150',
                invoiceid:params.invoice_id
            }),
            invoiceid:params.invoice_id
        })
    },
    setupController(controller, model) {
        controller.set('cards', model.card);
        controller.set('invoiceid', model.invoiceid);
        controller.set('invoice', model.invoice);
        this._super(controller, model);
    }
});


