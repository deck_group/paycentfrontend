import Mixin from '@ember/object/mixin';

export default Mixin.create({
    validateYear: function (value, regexformat, label) {
        var msgArray = []
        msgArray.push(this.formatRule(value, regexformat, label))
        msgArray.push(this.expmonthlength(value, label))
        msgArray.push(this.expyear(value, label))

        var filtered = msgArray.filter(function (el) {
            return el != "";
        });

        return filtered
    },
    formatRule(value, regexformat, label) {
        var msg = ''
        var patt = new RegExp(regexformat)
        if (patt.test(value) === false) {
            msg = label + ` format is not correct`
        }
        return msg

    },

    expmonthlength(value, label) {
        var month = value
        var msg = ''
        if (month.length !== 2) {
            msg = `Year must be YY , please check the ` + label
        }
        return msg
    },
    expyear(value, label) {
        var msg = ''
        var year = new Date().getFullYear();
        year = year.toString().substr(2, 2);
        if (value <= year) {
            msg = `Not Valid ` + label
        }
        return msg
    }

});
