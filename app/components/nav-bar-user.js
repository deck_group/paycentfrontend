import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    store: service(),
    router: service(),
    allpub: service('alllog'),
    loginpub: service('loginlog'),

    invoiceicon: `left:610px`,
    styleIcon: ``,
    classIcon: `vs-header-indicator-icon vs-header-hidden`,
    classinvoice: `menu-item-control`,
    classsubInvoice: `vs-header-sub-menu-wrapper vs-header-collapsed`,
    classtwo: `menu-item-control`,
    classthree: `menu-item-control`,
    classfour: `menu-item-control`,
    classfive: `menu-item-control`,
    classsix: `menu-item-control`,
    classIconCompute: computed('classinvoice', function () {
        return this.get('classinvoice')
    }),

    classsubInvoiceCompute: computed('classsubInvoice', function () {
        return this.get('classsubInvoice')
    }),

    styleIconCompute: computed('classinvoice', function () {
        return this.get('classinvoice')
    }),

    classinvoiceCompute: computed('classinvoice', function () {
        return this.get('classinvoice')
    }),

    classtwoCompute: computed('classtwo', function () {
        return this.get('classtwo')
    }),
    classthreeCompute: computed('classthree', function () {
        return this.get('classthree')
    }),
    classfourCompute: computed('classfour', function () {
        return this.get('classfour')
    }),
    classfiveCompute: computed('classfive', function () {
        return this.get('classfive')
    }),

    classsixCompute: computed('classsix', function () {
        return this.get('classsix')
    }),
    actions: {
        logout() {
            this.set('styleIcon', '')
            // this.set('classIcon', 'vs-header-indicator-icon vs-header-hidden')
            // this.set('classsubInvoice', 'vs-header-sub-menu-wrapper vs-header-collapsed')
            // this.set("classinvoice", 'menu-item-control')
            this.set("classtwo", 'menu-item-control')
            this.set("classthree", 'menu-item-control')
            this.set("classfour", 'menu-item-control')
            this.set("classfive", 'menu-item-control')
            this.set("classsix", 'menu-item-control vs-header-active')


            var dict = {}
            dict['userid'] = localStorage.getItem('user')
            dict['username'] = localStorage.getItem('username')
            dict['userrole'] = localStorage.getItem('role')
            dict['activitycat'] = "logout"
            dict['activitysubcat'] = "N/A"
            dict['status'] = "success"
            dict['statuscode'] = 200
            dict['variablebefore'] = { 'loginid': localStorage.getItem('user'), 'password': localStorage.getItem('password') }
            dict['variableafter'] = { 'loginid': localStorage.getItem('user'), 'password': localStorage.getItem('password') }
            dict['comment'] = 'N/A'
            dict['reason'] = 'success'
            dict['reasoncode'] = 200
            this.get("allpub").alllog(dict)


            var dicttow = {}
            dicttow['userid'] = localStorage.getItem('user')
            dicttow['username'] = localStorage.getItem('username')
            dicttow['userrole'] = localStorage.getItem('role')
            dicttow['activitytype'] = "logout"
            dicttow['inputuserid'] = 'N/A'
            dicttow['inputpassword'] = 'N/A'
            dicttow['lat'] = 'N/A'
            dicttow['lng'] = 'N/A'
            dicttow['ip'] = 'N/A'
            dicttow['status'] = "success"
            dicttow['statuscode'] = 200
            dicttow['variablebefore'] = { 'loginid': localStorage.getItem('user'), 'password': localStorage.getItem('password') }
            dicttow['variableafter'] = { 'loginid': localStorage.getItem('user'), 'password': localStorage.getItem('password') }
            dicttow['comment'] = 'N/A'
            dicttow['reason'] = 'success'
            dicttow['reasoncode'] = 200
            this.get("loginpub").loginpub(dicttow)
            localStorage.setItem('token', undefined)
            localStorage.setItem('user', undefined)
            localStorage.setItem('username', undefined)
            localStorage.setItem('password', undefined)
            localStorage.setItem('role', undefined)
            localStorage.setItem('merchantid', undefined)

            this.get('router').transitionTo('merchantsignin')
        },
    }
});
