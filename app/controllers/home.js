import Controller from '@ember/controller';

export default Controller.extend({
    actions:{
        newCard(){
            this.transitionToRoute("payment")
        }
    }
});
