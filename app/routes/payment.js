import Route from '@ember/routing/route';
import {
    hash
} from 'rsvp';

export default Route.extend({
    model() {
        return hash({
            card: this.store.findAll('cardlist')
        })

    },

    setupController(controller, model) {
        controller.set('cards', model.card);
        this._super(controller, model);
    }
});
