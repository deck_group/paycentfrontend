import Component from '@ember/component';
import { inject as service } from '@ember/service';


export default Component.extend({
    tagName: `div`,
    classNames: [`col-sm-12 form-group maginsome`],
    classInput: `input-txt-two`,
    labeltag: ``,
    label: ``,
    isVisa: false,
    isAmex: false,
    data: undefined,
    bankid:9999,
    bus: service("pubsub"),
    init() {
        this._super();
        this.get('bus').on('recievecardtype', this, this.recivedType);
    },
    recivedType(datatype,bankid) {
        this.set('bankid',bankid)
        if (datatype === "4") {
            this.set("isVisa", true)
            this.set("isAmex", false)
            this.set("data", 4)
        }
        else if (datatype === "37") {
            this.set("isVisa", false)
            this.set("isAmex", true)
            this.set("data", 37)
        } else {
            this.set("isAmex", false)
            this.set("isVisa", false)
        }
    },
    actions: {
        determinecard(data) {
            if (data === "4") {
                this.set("isVisa", true)
                this.set("isAmex", false)
            }
            else if (data === "37") {
                this.set("isVisa", false)
                this.set("isAmex", true)
            } else {
                this.set("isAmex", false)
                this.set("isVisa", false)
            }

        }
    }
});
